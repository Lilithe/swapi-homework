import React, { Component } from 'react';
import './Header.css';

export default class Header extends Component {
  render() {
    return (
        <header className="App-header">
            <div className="container-fluid nav nav-pills nav-fill bg-dark">
                <h1 className='text-warning'>StarWars</h1>
            </div>
        </header>
        );
  }

}