import React, { useState, useEffect, useCallback } from "react";
import Pagination from "../Pagination/Pagination";
import '../PeopleList/PeopleList.css'

export default function PeopleList(props) {
const [page, setPage] = useState(1)
let [people, setPeople] = useState([]);
const [error, setError] = useState(null);

const [currentPage, setCurrentPage] = useState(1);
  let NUM_OF_RECORDS = people.length;
  let LIMIT = 5;

  const onPageChanged = useCallback(
    (event, page) => {
      event.preventDefault();
      setCurrentPage(page);
    },
    [setCurrentPage]
  );
  people = people.slice(
    (currentPage - 1) * LIMIT,
    (currentPage - 1) * LIMIT + LIMIT
  );

let res;
useEffect(() => {
    if (!error) {
        fetch(`https://swapi.dev/api/people/?page=${page}`)
          .then(res => {
              if (res.status !== 404)
                 return res.json()
                  .then((result) => {
                      setPeople(prev => prev.concat(result.results)); 
                      setPage(prev => prev + 1);
                  })
              })
          
          .catch((err) => {
              setError(res.err);
          })
    }
}, );
return (
    <div>
      {people.map((person,index) =>
      
      <div className="card bg-warning">
        <div className="card-body">
          <h5 className="card-title">Meet our hero</h5>
          <p className="card-text" key={index}  data-toggle="modal" data-target="#exampleModal" > 
              {person.name} 
          </p>
          <button type="button" className="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-sm"
            onClick={async() => {
              let personalData = document.getElementsByClassName('my-table');
              if(personalData[index].style.display === "block") {
                personalData[index].style.display = "none";
              } else {
                personalData[index].style.display = "block";
              }
            }
            }
            >Some info about our hero</button>
            <div>
              <table className="my-table">
                <thead>
                  <tr>
                    <th scope="col">Gender</th>
                    <th scope="col">Birth Year</th>
                    <th scope="col">Eye Color</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{people[index].gender}</td>
                    <td>{people[index].birth_year}</td>
                    <td>{people[index].eye_color}</td>
                  </tr>
                </tbody>
              </table>
        </div>
        </div>
        
      </div>
      )}

      <div className="pagination-wrapper">
        {console.log(<Pagination
            totalRecords={NUM_OF_RECORDS}
            pageLimit={LIMIT}
            pageNeighbours={2}
            onPageChanged={onPageChanged}
            currentPage={currentPage}
          />)}
          <Pagination
            totalRecords={NUM_OF_RECORDS}
            pageLimit={LIMIT}
            pageNeighbours={2}
            onPageChanged={onPageChanged}
            currentPage={currentPage}
          />
        </div>
    </div>
)
}



// import Item from '../Items/Items.js';
// import './PeopleList.css'

// class PeopleList extends Component {
//     state = {
//         data: [],
//         loading: false,
//         currentPage: 1,
//         postsPerPage: 5
//     }
//     componentDidMount() {
//       for(let i = 1; i < 2; i++) {
//           getData(`https://swapi.dev/api/people/?page=${i}&format=json`)
//           .then((data) => this.setState({data: data.results}))
//           .finally(() => {
//           this.setState({loading: false})
//           })
//       }        

//     }   
//     render() {
//         const {loading, i} = this.state;
//         const indexOfLastPost = this.state.currentPage * this.state.postsPerPage;
//         const indexOfFirstPost = indexOfLastPost - this.state.postsPerPage;
//         const currentPosts = this.state.data.slice(indexOfFirstPost, indexOfLastPost);
//         const pageNumbers = [];
//         const setPage = (pageNum) => {
//             this.setState({currentPage: pageNum})
//           }
          
//     for (let i = 1; i <= Math.ceil(this.state.data.length / this.state.postsPerPage); i++) {
//       pageNumbers.push(i);
//     }
//         if(loading) return <span>Loading...</span>
//         return (
//         <div  className='Page-columns'>
//           <div className='Page-rows col'>
//             {
//                 currentPosts.map((item, i) => (
//                    <Item key={i} id={i + 1} item={item}/>
//                 ))
//             }
//             </div>
//            <nav aria-label="Page navigation example">
//               <ul className="pagination">
//               {
//                 pageNumbers.map((pageNum,  i) => (
//                   <li key={i} id={i + 1}  onClick={() => {setPage(pageNum)}} className={pageNum === this.state.currentPage ? " page-item cursor-pointer flex items-center justify-center w-12 h-12 border-2 rounded-full bg-blue-500 active" : "page-item cursor-pointer flex items-center justify-center w-12 h-12 border-2 rounded-full"}>
//                     <a className="page-link" href="#">{pageNum}</a>
//                   </li>
//                 ))
//               }
//               </ul>
//            </nav>
//          </div>
//         )
//     }
    
// };
// export default PeopleList;
  
